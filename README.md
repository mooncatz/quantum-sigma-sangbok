# Quantums och Sigmas sångbok

> **OBS: Vi arbetar på svenska, dvs. skriv alla commit messages på svenska.**

Här skrivs [Quantums](http://web.abo.fi/karen/special/squantum/) och [Sigmas](http://web.abo.fi/karen/special/sigma/) nya gemensamma sångbok.

[[_TOC_]]

# Kom igång

```zsh
git clone git@gitlab.com:mooncatz/quantum-sigma-sangbok.git
```

eller

```zsh
git clone https://gitlab.com/mooncatz/quantum-sigma-sangbok.git
```

# Git push och användning av branches

Per default är `master` branchen skyddad vilket betryder att det inte går att pusha förändringar till den. Det här är för att undvika konflikter i sångbokens innehåll då vi är flera som arbetar på den parallellt. Istället jobbar vi med det som kallas branches (förgreningar).

Ett exempel: Säg att vi vill göra en förändring till Undulaten. Stegen för att göra detta är

1. [Skapa en ny branch](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#branches) med ett beskrivande namn, t.ex. `undulaten`.
2. [Gör förändringarna](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-and-commit-local-changes) till sången i `undulaten` branchen.
3. [Öppna en merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#creating-merge-requests) för att merge (slå samman) förändringarna i `undulaten` branchen med `master` branchen. (Behändigt och bra praxis att utse code reviewers i samband med merge requesten)
4. Då de föreslagna förändringarna i merge requesten är godkända kan de som har maintainer roll merge `undulaten` branchen med `master` branchen.

# Git för nybörjare

Git är ett versionshanteringssystem för att spåra (eng. track) förändringar i en uppsättning filer. Git är ett kommandotolkprogram och är mycket enkelt att använda.

Git finns färdigt installerat på MacOS och de flesta Linux operativsystem. För Windows finns git att ladda ner [här](https://git-scm.com/download/win). Se [Microsofts instruktioner](https://docs.microsoft.com/en-us/devops/develop/git/install-and-set-up-git).

## `git clone` - ladda ner projektet (görs en gång)

Öppna kommadotolken:

- på Linux och MacOS ofta kallad Terminal (använder bash, zsh, e. dyl.)
- på Widnows öppna PowerShell, command prompt, e. dyl.

Navigera till det direktoratet ("foldern") som du vill ha projektet i. Här används `~/Dev` som exempel

```bash
cd Dev
```

Ladda ner (klona) projektet

```bash
git clone git@gitlab.com:mooncatz/quantum-sigma-sangbok.git
```

eller (ifall du inte har satt upp ssh autentisering till gitlab)

```bash
git clone https://gitlab.com/mooncatz/quantum-sigma-sangbok.git
```

## `git config` - konfigurera git (görs en gång)

Konfigurera ditt namn (eller användarnamn) din epost. Git bifogar denna information till förändrigar som du gör så att andra kan se vilka vem som gjort vilka förändringar.

```bash
cd quantum-sigma-sangbok
git config user.name kalleanka
git config user.email kalleanka@ankeborg.com
```

## `git status`, `git add`, `git commit` - registrera förändringar

Se vilka filer som har modifierats men vars modifierigar inte ännu har commit:ats (registrerats):

```bash
git status
```

Lägg till (stage) en förändring att inkluderas i följande commit:

```bash
git add 2010-Sangbok/kapitel/000snapsen.tex
git add 2010-Sangbok/bilder/halvan.png
```

Commit förändringen med ett kort och beskrivande meddelande:

```bash
git commit -m "Ny bild till helan"
```

## Git push - publicera förändringarna

Publicera dina förändringar till den gemensamma servern (dvs. vårt projekt på gitlab.com):

```bash
git push
```

## Gör flera minimala commits istället för få och stora commits

Kom ihåg att göra flera små commits iställer för några stora commits. Några orsaker är:

- Det blir betydligt lättare att spåra förändringar (speciellt om något gått fel, exempelvis så att .tex filen inte längre skulle kompilera).
- Det är lättare att förstå vad andra har gjort.
- En förändring hålls alltid i ett "paket".
- Ifall en liten förändring blivit fel behöver vi endast återställa (revert/undo) den enskilda lilla commiten, istället för att återställa en stor commit där största delen av innehållet förövrigt är korrekt.

## Exempel

Säg att vi vill skriva om kapitel 1 och göra följande förändringar:

- Ta bort några sånger
- Lägga till några sånger
- Modifiera några sånger
- Lägga till nya bilder

Alla förändringar kan förvisso göras i ett kör men det blir snabbt väldigt otydligt vad som ingår i en commit och att hitta i vilken commit någon förändring gjorts. Därför är det best practice att göra flera minimala "self contained" commits.

Gör såhär:

1. Gör en ny branch för att atbeta på föränringarna i:
   ```bash
   git checkout -b kapitel-1
   ```
2. Ta bort sånger från kapitel 1 (t.ex. kommentera bort dem i .tex filen istället för att radera dem), sedan gör en commit:
   ```bash
   git add 2010-Sangbok/kapitel/000Snapsen.tex
   git commit -m "Kommenterade bort 23 snapsvisor"
   ```
3. Lägg till nya sånger i .tex filen, sedan gör en commit
   ```bash
   git add 2010-Sangbok/kapitel/000Snapsen.tex
   git commit -m "Lade till Sigmas snapsvisor"
   ```
4. Skriv om några sånger, sedan gör en commit:
   ```bash
   git add 2010-Sangbok/kapitel/000Snapsen.tex
   git commit -m "Skrev om rasistiska snapsvisor"
   ```
5. Lägg till bya bilder:
   ```bash
   git add 2010-Sangbok/kapitel/000Snapsen.tex
   git add 2010-Sangbok/bilder/halvan.png
   git add 2010-Sangbok/bilder/dansa-macabre.png
   git add 2010-Sangbok/bilder/tomtegubbar.png
   git commit -m "Nya bilder"
   ```
6. Slutligen publicera förändringarna till `kapitel-1` branchen:
   ```bash
   git push
   ```

# GitLab Web IDE

Om man är hopplös på att använda git kan man också använda online editorn.  
<img src="readme-webide.png" width="70%" alt="GitLab Web IDE">
