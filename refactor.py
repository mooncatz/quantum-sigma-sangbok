import copy
import glob
import os
import re
from pathlib import Path
from typing import Dict


def _read(path: Path, verbose: bool = False) -> str:
    with open(path) as file:
        if verbose:
            print(f"reading {path}")
        file_as_string = ""
        for line in file:
            file_as_string += line
    return file_as_string


def _write(string: str, path: Path, verbose: bool = False) -> None:
    os.makedirs(path.parent, exist_ok = True)
    with open(path, "w+") as file:
        if verbose:
            print(f"writing: {path}")
        num_chars_written = file.write(string)
    return


def _filename(string: str) -> str:
    string = string.lower()
    translation_table = string.maketrans("åäö. ", "aao__")
    string = string.translate(translation_table)
    return string


def refactor(path: Path, verbose: bool = False) -> Dict[str, Dict[str, str]]:
    text = _read(path, verbose)

    text = text.strip()
    text = text.replace("\\newpage", "")   
    text = text.replace("\n\n\n", "\n\n")
    split_at = "\\begin{center}\n\\index"
    songs = text.split(split_at) 
    songs = songs[1:]

    data = {}

    song_name_pattern = re.compile("\\\\index{(.*)}")  # Captures everything between "\index{" and "}"
    for song in songs:
        song = song.strip()
        song = split_at + song

        match = re.search(song_name_pattern, song)
        song_name = match.group(1)  # We want the second subgroup of the match
        song_filename = _filename(song_name)
        
        data[song_filename] = {
            "title": song_name,
            "latex": song
        }

    return data


def run(ignore_files_with_prefix: str, verbose: bool = False):
    # Find all tex files in the current directory and sub-directories
    file_paths = glob.glob("**/kapitel/*.tex", recursive=True)
    file_paths = [Path(path) for path in file_paths]
    file_paths = [
        path for path in file_paths 
        if not path.name.startswith(ignore_files_with_prefix)
    
    ]

    songs = {}
    for path in file_paths:
        songs = {**songs, **refactor(path, verbose=verbose)}
    for filename, song in songs.items():
        path = Path(f"refactored/sanger/2010/{filename}.tex")
        _write(string=song["latex"], path=path, verbose=verbose)

    return


if __name__ == "__main__":
    run(ignore_files_with_prefix="ny", verbose=True)