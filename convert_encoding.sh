#!/usr/bin/env bash

# This bash script converts text from one encoding to another encoding.
#
# Author: Otto

FROM_ENCODING=iso-8859-1
TO_ENCODING=utf-8

# Get relative paths to all .tex files in sub-directories
files=($(find . -type f | grep "\.tex$"))
for f in ${files[@]}; do
    printf "\nConverting from %s to %s: %s" $FROM_ENCODING $TO_ENCODING $f;
    # Convert the encoding and overwrite the file, using a temporary 
    # intermediate file
    iconv -f $FROM_ENCODING -t $TO_ENCODING $f >> ${f}.temp;
    mv ${f}.temp $f;
done
