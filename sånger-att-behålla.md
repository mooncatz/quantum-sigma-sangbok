[[_TOC_]]

# Av tradition

Längtan till landet  
Modermålets sång  
O, gamla klang och jubeltid  
Studentsången  
Valborg är kommen  
Vårt Land  
Vintern rasat  

# Helan och halvan

Helan:  Helan går  
Halvan: Helan så ensam, Helan rasaat  
Tersen: Fjärran han dröjer  
Kvarten: Tersen var bra  
Kvinten: I finlands djupa skogar  
Imbelupet  
Gubben Noak  

# Snapsvisor


Alkoholen  
Blott en fest  
Bohemian snapsody  
Bordeaux, Bordeaux  
Brand i stugan  
Brännvin, vatten  
Byssan lull  
Danse Macabre  
Den relativa supen  
Det var i ungdomens fagraste vår  
Dirlan Da  
Du gamla du fina  
E=mc^2  
En gång i månan  
En kanna öl  
En kårenit  
Fredmans sång nr. 21  
Fyllebjörnarna?  
Grannens skithuus?  
Härjarevisan  
Imsig vimsig (Q)  
Internationalen  
Jag har aldrig vart på snusen  
Jag ska festa  
Jag var full en gång?  
Kassen full  
Krapula (Q)  
Lapin Kulta  
Liten jumbo  
Liten och besk  
Livet är härligt  
Magen brummar  
Måsen  
Matematikvisan / än kan vi skilja  
Mattestudier  
Metanol  
Minnet  
Mistä löytyy Karjala?  
Mitt lilla lån  
Muminsnaps  
Mystiskt försvinnare  
När gäddorna leka  
Nu skålar vi Quantum till  
Nu tar vi den  
Ordet till ölet  
Partikelvisan  
Quantum-kvädet (Q)  
Rövarvisan  
Rullaati  
Så länge rösten är mild  
Sädesbrännvin (52 OCH 58 i Q)  
Siffervisan +1  
Siffervisan/Nykterhetstestet  
Sjösala vals  
Ska vi snapsa  
Snilledrag  
Spritbolaget  
Strejk på Koff  
Stugan den står upp och ner (Q)  
Sudda sudda (Q)  
Tänk om jag hade lilla nubben  
Tänk om jag inte var så tråkig  
Till supen så tager man sill  
Tittar det snöar  
Törsten rasar  
Traktor Alban  
Undulaten  
Uti Finland dricks ej vatten  
Uti min mage  
Välkomstvisa  
Värdinnans sång  
Värdinnans skål  
Vårtorka  
Vem kan ragla  
Vi ska supa  
Vi tar (S)  
Vi vill ha mera öl    
Vodka Vodka  

# Korta visor

Första Jukkasjärvivisan  
Andra Jukkasjärvivisan  
Där som sädesfälten  
Hej vet ni va  
Hell and Gore  
Ingmar Bergmans  
Måsen - Introt  
Nu  
Vad i all sin dar  

# Nationsvisor

## Sibbo
Sibbovisan

## ÖN
Jag trivs bäst i Österbotten
Kåtongfields

## ÅSL
Hej Pallervantar
Peter Holms visa

## Öffen

## NN

## Nyco
Gudars punsch?

## Hangögillet

# Julvisor

Bella Notte  
Blue Christmas  
Hej tomtegubbar  
I koma  
Jag såg mamma kyssa tomten  
Nu är jul igen  
Rudolf med röda mulen + Fredriks extra  
Rudolph the red-nosed raindeer  
Sigmajul i ett rus  
Tarzans julvisa  
Tipp Tapp  
Tre pepparkaksgubbar  
Vi gubbar  
Vi tar ett rus  

# Punchen

Alla sorters punsch  
Crambamboli  
Djungelpunsch  
Festus punschvisa  
Kaffevisan  
Punschen  
Punschen rinner genom strupen  
Punschens tillkomst  
Sista punschvisan  

# Censur

En del säger den är liten  
Feta fransyskor  
Självmördarvisan  
Theobald Thor  
Yogi Bear  
Är du ensam ikväll (?)  


# Inhemskt och uthemskt

# Annat

Aurumvisa?  
Dårhuset-visa?  
Gadovisa?  
Pidrovisa?  
Pidrosidor (VI/DE)  
